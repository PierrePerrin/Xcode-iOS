//
//  ViewController.swift
//  Xcode-iOS
//
//  Created by Pierre Perrin on 03/08/2019.
//  Copyright © 2019 Pierre Perrin. All rights reserved.
//

import UIKit
import Highlightr
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var textEdit: UITextView!
    
    var disposeBag = DisposeBag()
    let highlightr = Highlightr()
    
    var codeStorage = BehaviorRelay<NSAttributedString?>.init(value: nil)
    var currentTheme = BehaviorRelay<Theme?>.init(value: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        bindCodeAndTextView()
        bindTheme()
    }
    
    func bindCodeAndTextView() {
        
        codeStorage.distinctUntilChanged()
            .bind(to: textEdit.rx.attributedText)
            .disposed(by: disposeBag)
        
        textEdit.rx
            .text
            .observeOn(SerialDispatchQueueScheduler.init(qos: .default))
            .map {self.highlightr?.highlight($0 ?? "", as: "swift")}
            .bind(to: codeStorage)
            .disposed(by: disposeBag)
        
        textEdit.text = ""
    }
    
    func bindTheme() {
        
        currentTheme.subscribe(onNext: { (theme) in
            self.textEdit.backgroundColor = theme?.themeBackgroundColor
            self.highlightr?.setTheme(to: theme?.theme ?? "")
            
            // Here we update the attributed string attributes according to the new theme
            self.textEdit.text = self.textEdit.text
        })
            .disposed(by: disposeBag)
        
        currentTheme.accept(Theme.init(themeString: highlightr?.availableThemes().randomElement() ?? ""))

    }
}
